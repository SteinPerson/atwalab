package com.fainshtein.lab2;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SecondLab {

  private static final String BASE_URL = "https://www.nmu.org.ua/ua/";
  private static final String XPATH = "/html/body/center/div[4]/div/div[1]/ul/li[4]/a";
  private WebDriver webDriver;

  @BeforeClass(alwaysRun = true)
  public void setUp() {
    WebDriverManager.firefoxdriver().setup();
    FirefoxOptions firefoxOptions = new FirefoxOptions();
    firefoxOptions.addArguments("--start-fullscreen");
    firefoxOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
    this.webDriver = new FirefoxDriver(firefoxOptions);
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() {
    webDriver.quit();
  }

  @BeforeMethod
  public void preconditions() {
    webDriver.get(BASE_URL);
  }

  @Test
  public void testHeaderExists() {
    WebElement header = webDriver.findElement(By.id("heder"));

    Assert.assertNotNull(header);
  }

  @Test
  public void testClickOnForStudent() {
    WebElement forStudentButton = webDriver.findElement(
        By.xpath("/html/body/center/div[4]/div/div[1]/ul/li[4]/a"));

    forStudentButton.click();

    Assert.assertNotNull(forStudentButton);
    Assert.assertNotEquals(webDriver.getCurrentUrl(), BASE_URL);
  }

  @Test
  public void testSearchFieldOnForStudentPage() {
    String studentPageUrl = "content/student_life/students/";
    webDriver.get(BASE_URL + studentPageUrl);
    WebElement searchButton = webDriver.findElement(By.tagName("input"));
    searchButton.click();
    WebElement parentElement = webDriver.findElement(
        By.xpath("/html/body/center/div[4]/div/table/tbody/tr/td[2]"));
    WebElement searchField = parentElement.findElement(By.tagName("input"));
    System.out.println(String.format("Name attribute: %s", searchField.getAttribute("name")) +
        String.format("\nID attribute: %s", searchField.getAttribute("id")) +
        String.format("\nType attribute: %s", searchField.getAttribute("type")) +
        String.format("\nValue attribute: %s", searchField.getAttribute("value")) +
        String.format("\nPosition: (%d;%d)", searchField.getLocation().x,
            searchField.getLocation().y) +
        String.format("\nSize: %dx%d", searchField.getSize().height, searchField.getSize().width));
    String inputValue = "I need info";

    searchField.sendKeys(inputValue);
    searchField.sendKeys(Keys.ENTER);

    Assert.assertNotNull(searchField);
    Assert.assertNotEquals(webDriver.getCurrentUrl(), studentPageUrl);
    Assert.assertEquals(searchField.getAttribute("value"), inputValue);
  }

  @Test
  public void testSlider() {
    WebElement nextButton = webDriver.findElement(By.className("next"));
    WebElement nextButtonByCss = webDriver.findElement(By.cssSelector("a.next"));

    Assert.assertEquals(nextButton, nextButtonByCss);

    WebElement previousButton = webDriver.findElement(By.className("prev"));

    for (int i = 0; i < 20; i++) {
      if (nextButton.getAttribute("class").contains("disabled")) {
        for (int j = 0; j < 5; j++) {
          previousButton.click();
        }

        Assert.assertTrue(previousButton.getAttribute("class").contains("disabled"));
        Assert.assertFalse(nextButton.getAttribute("class").contains("disabled"));
      } else {
        for (int j = 0; j < 5; j++) {
          nextButton.click();
        }

        Assert.assertTrue(nextButton.getAttribute("class").contains("disabled"));
        Assert.assertFalse(previousButton.getAttribute("class").contains("disabled"));
      }
    }
  }

}
