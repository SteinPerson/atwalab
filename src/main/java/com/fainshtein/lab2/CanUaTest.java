package com.fainshtein.lab2;

import static com.fainshtein.lab2.constants.TestConstants.CHOOSE_CITY_TEXT_CLASS;
import static com.fainshtein.lab2.constants.TestConstants.CITIES_LIST_BLOCK_CLASS;
import static com.fainshtein.lab2.constants.TestConstants.CLASS_ATTRIBUTE;
import static com.fainshtein.lab2.constants.TestConstants.INPUT_TAG;
import static com.fainshtein.lab2.constants.TestConstants.SEARCH_BLOCK_ID;
import static com.fainshtein.lab2.constants.TestConstants.VISIBLE_CLASS;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CanUaTest {

  private static final String BASE_URL = "https://can.ua/";
  private static final int IMPLICIT_WAIT_TIMEOUT = 15;
  private static final int ACTION_EXECUTE_TIMEOUT = 10;
  private static final int NUMBER_OF_REPETITIONS = 4;
  private WebDriver webDriver;

  @BeforeClass(alwaysRun = true)
  public void setUp() {
    WebDriverManager.firefoxdriver().setup();
    FirefoxOptions firefoxOptions = new FirefoxOptions();
    firefoxOptions.setImplicitWaitTimeout(Duration.ofSeconds(IMPLICIT_WAIT_TIMEOUT));
    this.webDriver = new FirefoxDriver(firefoxOptions);
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() {
    webDriver.quit();
  }

  @BeforeMethod
  public void preconditions() {
    webDriver.get(BASE_URL);
  }

  @Test
  public void testClickOnPCComponentsButton() {
    WebElement asideMenu = webDriver.findElement(By.id("aside"));
    List<WebElement> menuLinks = asideMenu.findElements(By.className("menu__link"));
    WebElement componentsButton = null;
    for (WebElement menuLink : menuLinks) {
      if (menuLink.getText().equals("Комплектуючі для ПК")) {
        componentsButton = menuLink;
      }
    }
    Assert.assertNotNull(componentsButton);

    componentsButton.click();

    Assert.assertNotEquals(webDriver.getCurrentUrl(), BASE_URL);
  }

  @Test
  public void testSearchFieldOnMainPage() {
    WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(ACTION_EXECUTE_TIMEOUT));
    WebElement searchForm = webDriver.findElement(By.id(SEARCH_BLOCK_ID));
    WebElement searchField = searchForm.findElement(By.tagName(INPUT_TAG));
    String inputValue = "rtx 3060";

    searchField.sendKeys(inputValue);
    searchField.submit();
    wait.until(webDriver -> webDriver.getCurrentUrl().contains("/search/?text="));

    webDriver.get(webDriver.getCurrentUrl());
    WebElement newSearchForm = webDriver.findElement(By.id(SEARCH_BLOCK_ID));
    WebElement newSearchField = newSearchForm.findElement(By.tagName(INPUT_TAG));

    Assert.assertNotNull(newSearchField);
    Assert.assertNotEquals(webDriver.getCurrentUrl(), BASE_URL);
    Assert.assertEquals(newSearchField.getAttribute("value"), inputValue);
  }

  @Test
  public void testCartExists_relativeXPath() {
    WebElement cart = webDriver.findElement(By.xpath("//*[@id=\"header-cart\"]"));

    Assert.assertNotNull(cart);
  }

  @Test
  public void testCartExists_id() {
    WebElement cart = webDriver.findElement(By.id("header-cart"));

    Assert.assertNotNull(cart);
  }

  @Test
  public void testCitiesBlockWidget_openClose() {
    WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(ACTION_EXECUTE_TIMEOUT));
    WebElement cityChooser = webDriver.findElement(By.className(CHOOSE_CITY_TEXT_CLASS));
    WebElement citiesBlock = webDriver.findElement(By.className(CITIES_LIST_BLOCK_CLASS));

    Assert.assertNotNull(cityChooser);
    Assert.assertNotNull(citiesBlock);

    for (int i = 0; i < NUMBER_OF_REPETITIONS; i++) {
      if (citiesBlock.getAttribute(CLASS_ATTRIBUTE).contains(VISIBLE_CLASS)) {
        wait.until(ExpectedConditions.elementToBeClickable(By.className(CHOOSE_CITY_TEXT_CLASS)));
        cityChooser.click();

        Assert.assertFalse(citiesBlock.getAttribute(CLASS_ATTRIBUTE).contains(VISIBLE_CLASS));
      } else {
        wait.until(ExpectedConditions.elementToBeClickable(By.className(CHOOSE_CITY_TEXT_CLASS)));
        cityChooser.click();

        Assert.assertTrue(citiesBlock.getAttribute(CLASS_ATTRIBUTE).contains(VISIBLE_CLASS));
      }
    }
  }
}
