package com.fainshtein.lab2.constants;

public final class TestConstants {

  public static final String CLASS_ATTRIBUTE = "class";
  public static final String VISIBLE_CLASS = "visible";
  public static final String CITIES_LIST_BLOCK_CLASS = "cities-list-block";
  public static final String CHOOSE_CITY_TEXT_CLASS = "choose-city-text";
  public static final String INPUT_TAG = "input";
  public static final String SEARCH_BLOCK_ID = "search_block";
}
