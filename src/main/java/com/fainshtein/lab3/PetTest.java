package com.fainshtein.lab3;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import java.util.Map;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PetTest {

  private static final String BASE_URL = "https://petstore.swagger.io/v2";

  private static final String PET_PATH = "/pet";
  private static final String PET_ID_PATH = PET_PATH + "/{petId}";

  private static final long ID = 233;
  private static final String CATEGORY = "Master";
  private static final String NAME = "Daniil Fainshtein";
  private static final String UPDATED_NAME = "D.F.";
  private static final String PHOTO_URL = "https://photo";
  private static final String TAG = "Student";
  private static final String STATUS = "available";

  @BeforeClass
  public void setUp() {
    RestAssured.baseURI = BASE_URL;
    RestAssured.defaultParser = Parser.JSON;
    RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON)
        .build();
    RestAssured.responseSpecification = new ResponseSpecBuilder().build();
  }

  @Test
  public void verifyCreateAction() {
    Map<String, ?> body = Map.of(
        "id", ID,
        "category", Map.of("name", CATEGORY),
        "name", NAME,
        "photoUrls", new String[]{PHOTO_URL},
        "tags", new Map[]{Map.of("name", TAG)},
        "status", STATUS
    );
    given().body(body)
        .post(PET_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK);
  }

  @Test(dependsOnMethods = "verifyCreateAction")
  public void verifyGetAction() {
    given().pathParam("petId", ID)
        .get(PET_ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .body("name", equalTo(NAME));
  }

  @Test(dependsOnMethods = "verifyCreateAction")
  public void verifyUpdateAction() {
    Map<String, ?> body = Map.of(
        "id", ID,
        "category", Map.of("name", CATEGORY),
        "name", UPDATED_NAME,
        "photoUrls", new String[]{PHOTO_URL},
        "tags", new Map[]{Map.of("name", TAG)},
        "status", STATUS
    );
    given().body(body)
        .put(PET_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .body("name", equalTo(UPDATED_NAME));
  }

  @Test(dependsOnMethods = "verifyCreateAction", priority = 1)
  public void verifyDeleteAction() {
    given().pathParam("petId", ID)
        .delete(PET_ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK);
  }
}
