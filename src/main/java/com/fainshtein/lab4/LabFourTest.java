package com.fainshtein.lab4;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import java.util.Map;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LabFourTest {

  private static final String BASE_URL = "https://f17b166a-38de-4470-81a1-0baa8595f5d0.mock.pstmn.io";
  private static final String FOLDER_SLUG = "/folder";
  private static final String FOLDER_NAME = "LabFolder";
  private static final String FOLDER_AUTHOR = "Daniil Fainshtein";
  private static final String USER_STATUS = "USER";
  private static final String ADMIN_STATUS = "ADMIN";

  @BeforeClass
  public void setUp() {
    RestAssured.baseURI = BASE_URL;
    RestAssured.defaultParser = Parser.JSON;
    RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON)
        .build();
    RestAssured.responseSpecification = new ResponseSpecBuilder().build();
  }

  @Test
  public void createFolder_adminStatus_folderCreated() {
    Map<String, ?> body = Map.of(
        "status", ADMIN_STATUS,
        "name", FOLDER_NAME,
        "author", FOLDER_AUTHOR
    );
    given().body(body)
        .post(BASE_URL + FOLDER_SLUG)
        .then()
        .statusCode(HttpStatus.SC_CREATED);
  }

  @Test
  public void createFolder_userStatus_folderIsNotCreated() {
    Map<String, ?> body = Map.of(
        "status", USER_STATUS,
        "name", FOLDER_NAME,
        "author", FOLDER_AUTHOR
    );
    given().body(body)
        .post(BASE_URL + FOLDER_SLUG)
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN)
        .and()
        .body("exception", equalTo("USER don't have permissions to create folder"));
  }

  @Test
  public void getFolder_nameParameterIsGiven_folderFound() {
    given().param("name", FOLDER_NAME)
        .get(BASE_URL + FOLDER_SLUG)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .body("name", equalTo(FOLDER_NAME))
        .and()
        .body("author", equalTo(FOLDER_AUTHOR));
  }

  @Test
  public void getFolder_nameParameterIsAbsent_folderNotFound() {
    given().param("name", "")
        .get(BASE_URL + FOLDER_SLUG)
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND)
        .and()
        .body("exception", equalTo("Folder not found"));
  }

  @Test
  public void updateFolder_folderNameIsGiven_folderUpdated() {
    Map<String, ?> body = Map.of(
        "status", USER_STATUS,
        "name", "New Folder",
        "author", FOLDER_AUTHOR
    );
    given().pathParam("name", FOLDER_NAME).body(body)
        .put(BASE_URL + FOLDER_SLUG + "/{name}")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .body("name", equalTo("New Folder"));
  }

  @Test
  public void updateFolder_folderNameIsAbsent_folderIsNotUpdated() {
    Map<String, ?> body = Map.of(
        "status", USER_STATUS,
        "name", "New Folder",
        "author", FOLDER_AUTHOR
    );
    given().pathParam("name", "").body(body)
        .put(BASE_URL + FOLDER_SLUG + "/{name}")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .and()
        .body("exception", equalTo("Folder name is required"));
  }

  @Test
  public void deleteFolder_folderNameIsGiven_folderDeleted() {
    given().pathParam("name", FOLDER_NAME)
        .delete(BASE_URL + FOLDER_SLUG + "/{name}")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .body("response", equalTo("Folder deleted"));
  }

  @Test
  public void deleteFolder_incorrectFolderName_folderNotFound() {
    given().pathParam("name", "Lab")
        .delete(BASE_URL + FOLDER_SLUG + "/{name}")
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND)
        .and()
        .body("exception", equalTo("Folder not found"));
  }
}
